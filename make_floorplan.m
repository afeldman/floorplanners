iterations=0;
l=fgetl(stdin); %read header
orig_scale=0;
while ischar(l)
    coords={};
    l=fgetl(stdin);
    while l(1)=='('
        comma = find(l==',');
        coord_x = str2num(l(2:comma-1));
        coord_y = str2num(l(comma+1:end-1));
        coords=[coords,{[coord_x,coord_y]}];
        l=fgetl(stdin);
    end
    dims={};
    l=fgetl(stdin);
    while l(1)=='('
        comma = find(l==',');
        w = str2num(l(2:comma-1));
        h = str2num(l(comma+1:end-1));
        dims=[dims,{[w,h]}];
        l=fgetl(stdin);
    end
    warning off all
    figure
    hold on
    max_x=0;
    max_y=0;
    label_strs={};
    for ind=1:length(coords)
        x = coords{ind}(1);
        y = coords{ind}(2);
        w = dims{ind}(1);
        h = dims{ind}(2);
        rect_x = [x,x+w,x+w,x,x];
        rect_y = [y,y,y+h,y+h,y];
        max_x=max([max_x,x+w]);
        max_y=max([max_y,y+h]);
        plot(rect_x,rect_y,'k','LineWidth',2)
        label_str=sprintf('text(%d+%d/2,%d+%d/2,num2str(%d-1),''HorizontalAlignment'',''center'',''FontSize'',8)',x,w,y,h,ind);
        label_strs=[label_strs,{label_str}];
    end
    axis square
    max_l=max([max_x,max_y]);
    if max_l<75
        for i=1:length(label_strs)
            eval(label_strs{i})
        end
    end
    axis nolabel
    axis([0,max_l,0,max_l])
    iterations=iterations+1;
    if iterations==1
        orig_scale=max_l;
    else
        if max_l<orig_scale
            axis([0,orig_scale,0,orig_scale])
        else
            axis([0,max_l,0,max_l])
        end
    end
    filename=sprintf('%d_floorplan.png',iterations);
    pause(0.1)
    print(filename);
end
