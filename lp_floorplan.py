#!/usr/bin/python2.7

##############################################################################################
# Copyright (c) 2011 Allan Feldman <allan.feldman(__AT__)gatech.edu>                         #
#                                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining                      #
# a copy of this software and associated documentation files (the "Software"),               #
# to deal in the Software without restriction, including without limitation the              #
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell           #
# copies of the Software, and to permit persons to whom the Software is furnished            #
# to do so, subject to the following conditions:                                             #
#                                                                                            #
# The above copyright notice and this permission notice shall be included in all copies      #
# or substantial portions of the Software.                                                   #
#                                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,        #
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   #
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE  #
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       #
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER     #
# DEALINGS IN THE SOFTWARE.                                                                  #
##############################################################################################

# Written By: Allan Feldman

import sys
import subprocess

def pack(dims):
    """solve the packing problem as a linear programming problem"""
    #get the upper bound on width and height
    ub=0
    for dim in dims:
        ub+=max(dim[0],dim[1])
    var_declarations=[]
    var_constraints = []
    w_constraints = []
    h_constraints = []
    overlap_constraints=[]
    
    #generate non-overlap constraints
    for i in xrange(len(dims)):
        h_i=dims[i][1]
        w_i=dims[i][0]
        for j in xrange(i+1,len(dims)):
            h_j=dims[j][1]
            w_j=dims[j][0]
            #constraint from block i to j
            left="x{0} + {1} z{0} + {2} - {2} z{0} <= x{3} + {4} x_{0}_{3} + {4} y_{0}_{3};".format(i,h_i,w_i,j,ub)
            right="x{0} + {1} z{0} + {2} - {2} z{0} <= x{3} + {4} - {4} x_{3}_{0} + {4} y_{3}_{0};".format(j,h_j,w_j,i,ub)
            bottom="y{0} + {1} z{0} + {2} - {2} z{0} <= y{3} + {4} + {4} x_{0}_{3} - {4} y_{0}_{3};".format(i,w_i,h_i,j,ub)
            top="y{0} + {1} z{0} + {2} - {2} z{0} <= y{3} + {5} - {4} x_{3}_{0} - {4} y_{3}_{0};".format(j,w_j,h_j,i,ub,2*ub)
            overlap_constraints.append(left+"\n")
            overlap_constraints.append(right+"\n")
            overlap_constraints.append(bottom+"\n")
            overlap_constraints.append(top+"\n")

            #binary variable xij & yij (position)
            bin_declaration_x="bin x_{0}_{1};".format(i,j)
            bin_declaration_y="bin y_{0}_{1};".format(i,j)
            var_declarations.append(bin_declaration_x+"\n"+bin_declaration_y+"\n")
        #binary variable declaration for block rotation
        bin_rot="bin z{0};".format(i)
        var_declarations.append(bin_rot+"\n")
        #variable constraints all x&y values >=0
        x_gt_0="x{0}>=0;".format(i)
        y_gt_0="y{0}>=0;".format(i)
        var_constraints.append(x_gt_0+"\n"+y_gt_0+"\n")
        #width constraint (x+w<Y) -> take into account rotation
        width_constraint="x{0} + {1} z{0} + {2} - {2} z{0} <= Y;".format(i,h_i,w_i)
        w_constraints.append(width_constraint+"\n")
        #height constraint (y+h<Y) -> same
        height_constraint="y{0} + {1} z{0} + {2} - {2} z{0} <= Y;".format(i,w_i,h_i)
        h_constraints.append(height_constraint+"\n")

    #objective - Y will be max(width,height): minimize
    pipe_me=["min: Y;\n"]
    pipe_me.extend(overlap_constraints)
    pipe_me.extend(w_constraints)
    pipe_me.extend(h_constraints)
    pipe_me.extend(var_constraints)
    #lp_solve requires variable declarations after the constraints
    pipe_me.extend(var_declarations)

    p=subprocess.Popen('./lp_solve',stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    result=p.communicate(''.join(pipe_me))
    if result[1]:
        sys.exit("lp_solve returned STDERR:{0}".format(result[1]))
    result=result[0].split('\n')

    #at this point, we know the max(w,h) bound.
    #we can choose h=max(w,h) and rerun to minimize width to get the absolute min area
    useful_part = result[4].index(' ')
    h_bound = int(result[4][useful_part:])
    h_constraints_str = ''.join(h_constraints)
    h_constraints_str=h_constraints_str.replace("Y",str(h_bound))

    #objective - Y will be min width: minimize
    pipe_me=["min: Y;\n"]
    pipe_me.extend(overlap_constraints)
    pipe_me.extend(w_constraints)
    #new h constraints
    pipe_me.append(h_constraints_str)
    pipe_me.extend(var_constraints)
    #lp_solve requires variable declarations after the constraints
    pipe_me.extend(var_declarations)

    p=subprocess.Popen('./lp_solve',stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    result=p.communicate(''.join(pipe_me))
    if result[1]:
        sys.exit("lp_solve returned STDERR:{0}".format(result[1]))
    result=result[0].split('\n')

    x_coords=[]
    y_coords=[]
    rotated=[]
    width=0
    height=0
    for i in xrange(5,len(result)-1):
        useful_part=result[i].index(' ')
        if result[i][1]!='_':
            dim_num=int(result[i][1:useful_part])+1
            coord=int(result[i][useful_part:])
            if result[i][0]=='x':
                x_coords.insert(dim_num,coord)
            elif result[i][0]=='y':
                y_coords.insert(dim_num,coord)
            else:
                rotated.insert(dim_num,coord==1)
    sort_ind=lambda x: sorted(range(len(x)),key=x.__getitem__)
    ind=sort_ind(x_coords)
    x_coords_s=[x_coords[i] for i in ind]
    rotated_s=[rotated[i] for i in ind]
    dims_s=[dims[i] for i in ind]
    x = x_coords_s[-1]
    rot = rotated_s[-1]
    dim = dims_s[-1]
    width = x + int(rot)*dim[1] + dim[0] - int(rot) * dim[0]

    ind=sort_ind(y_coords)
    y_coords_s=[y_coords[i] for i in ind]
    rotated_s=[rotated[i] for i in ind]
    dims_s=[dims[i] for i in ind]
    y = y_coords_s[-1]
    rot = rotated_s[-1]
    dim = dims_s[-1]
    height=y+int(rot)*dim[0] + dim[1] - int(rot)*dim[1]
        
    return (width*height, x_coords, y_coords,rotated)


if __name__ == '__main__':
    dims=[]
    expression=sys.stdin.readline().replace("\n","").replace("\r","")
    expression_v_h_removed=expression.replace("-V","").replace("-H","")
    num_blocks=max([int(x) for x in expression_v_h_removed.split('-')])+1
    for i in xrange(num_blocks):
        line = sys.stdin.readline()
        dim  = eval("("+line+")")
        dims.append(dim)
    (area,x_coords,y_coords,rotated) = pack(dims)
    print "Coordinates:"
    for x,y in zip(x_coords,y_coords):
        print (x,y)
    print "Dimensions:"
    for rot,dim in zip(rotated,dims):
        new_dim = list(dim)
        if rot:
            new_dim.reverse()
        print tuple(new_dim)
    print area
