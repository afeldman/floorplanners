#!/usr/bin/python2.7

##############################################################################################
# Copyright (c) 2011 Allan Feldman <allan.feldman(__AT__)gatech.edu>                         #
#                                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining                      #
# a copy of this software and associated documentation files (the "Software"),               #
# to deal in the Software without restriction, including without limitation the              #
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell           #
# copies of the Software, and to permit persons to whom the Software is furnished            #
# to do so, subject to the following conditions:                                             #
#                                                                                            #
# The above copyright notice and this permission notice shall be included in all copies      #
# or substantial portions of the Software.                                                   #
#                                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,        #
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   #
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE  #
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       #
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER     #
# DEALINGS IN THE SOFTWARE.                                                                  #
##############################################################################################

# Written By: Allan Feldman

from optparse import OptionParser
from stockmeyer import stockmeyer
from coord_gen import expression_to_coords
import math
import random
import time
import sys

def calculate_area(exp,block_dims):
    stack=[]
    for node in exp:
        if node!='V' and node!='H':
            node=int(node)
            stack.append(block_dims[node])
            continue

        right_dim=stack.pop()
        left_dim=stack.pop()
        
        if node=='V':
            this_node_dim = (left_dim[0] + right_dim[0], max(left_dim[1],right_dim[1]))
        else:
            this_node_dim = (max(left_dim[0],right_dim[0]),left_dim[1]+right_dim[1])
        
        stack.append(this_node_dim)
    dim=stack.pop()
    return (dim,block_dims)

cost_function=calculate_area

def m1(exp):
    new_exp=list(exp)
    is_block = lambda x: x!='V' and x!='H'
    node_num=0
    possibles=[]
    while node_num<len(new_exp)-1:
        if is_block(new_exp[node_num]):
            for j in xrange(node_num+1,len(new_exp)):
                if is_block(new_exp[j]):
                    possibles.append((node_num,j))
                    break
        node_num+=(j-node_num)
    swap = random.choice(possibles)
    t=new_exp[swap[0]]
    new_exp[swap[0]] = new_exp[swap[1]]
    new_exp[swap[1]] = t
    return new_exp

def m2(exp):
    new_exp=list(exp)
    is_block = lambda x: x!='V' and x!='H'
    chains=[]
    chain_start=2
    chain_end=0
    chain_started=False
    #note all polish expressions end in an operator and begin with 2 operands
    while chain_end<len(new_exp):
        if chain_started:
            if is_block(new_exp[chain_end]):
                chains.append((chain_start,chain_end))
                chain_started=False
                chain_start=chain_end+1
            else:
                chain_end+=1
        else:
            if is_block(new_exp[chain_start]):
                chain_start+=1
            else:
                chain_started=True
                chain_end=chain_start+1
    chains.append((chain_start,chain_end))

    #pick a random chain to invert
    inv_chain = random.choice(chains)

    for i in xrange(inv_chain[0],inv_chain[1]):
        if new_exp[i]=='V':
            new_exp[i]='H'
        else:
            new_exp[i]='V'
    return new_exp

def is_legal_exp(exp):
    num_operand=0
    num_operator=0
    for node in exp:
        if node=='V' or node=='H':
            num_operator+=1
        else:
            num_operand+=1
        if num_operator>=num_operand:
            return False
    return True

def m3(exp):
    is_block = lambda x: x!='V' and x!='H'
    
    possibles=[]
    #note it is never valid to have an operand on the end, so we don't enumerate that possibility
    for ind in xrange(len(exp)-2):
        node=exp[ind]
        next_node = exp[ind+1]
        #possible if node is a block and next node isn't or vice versa
        if (is_block(next_node) and not(is_block(node))) or (is_block(node) and not(is_block(next_node))):
            possibles.append(ind)

    while True:
        if len(possibles)==0:
            sys.exit("no valid m3 moves")
        new_exp=list(exp)
        swap_ind = random.choice(possibles)
        possibles.pop(possibles.index(swap_ind))
        t=new_exp[swap_ind]
        new_exp[swap_ind] = new_exp[swap_ind+1]
        new_exp[swap_ind+1]=t
        if is_legal_exp(new_exp):
            break

    return new_exp

def get_initial_temperature(exp,block_dims,acceptance_prob=90):
    ((init_width,init_height),garbage) = cost_function(exp,block_dims)
    initial_area = init_width*init_height
    exp=m1(exp)
    ((m1_w,m1_h),garbage) = cost_function(exp,block_dims)
    m1_area = m1_w * m1_h
    exp=m2(exp)
    ((m2_w,m2_h),garbage) = cost_function(exp,block_dims)
    m2_area = m2_w * m2_h
    exp=m3(exp)
    ((m3_w,m3_h),garbage) = cost_function(exp,block_dims)
    m3_area = m3_w * m3_h

    m1_delta = abs(initial_area-m1_area)
    m2_delta = abs(m1_area-m2_area)
    m3_delta = abs(m2_area-m3_area)

    del_avg = (float(m1_delta) + float(m2_delta) + float(m3_delta))/3
    return -del_avg / math.log(float(acceptance_prob)/100)

def anneal(exp,t0,eps,r,N,reject_ratio,desired_ratio, ratio_importance, block_dims, max_time=100):
    time_start=time.time()
    ((best_w,best_h),best_dims) = cost_function(exp,block_dims)
    #ensure aspect ratio always between 0 and 1
    num = float(min(best_w,best_h))
    den = float(max(best_w,best_h))
    best_cost=best_w*best_h+abs(num/den-desired_ratio)*float(1)/desired_ratio*ratio_importance*best_w * best_h
    best_exp = exp
    moves = [m1,m2,m3]
    t=t0
    while True:
        total_moves = 0
        uphill = 0
        reject = 0
        while True:
            move = random.choice(moves)
            new_exp = move(exp)
            total_moves += 1
            ((w,h),new_dims) = cost_function(new_exp,block_dims)
            #ensure aspect ratio always between 0 and 1
            num = float(min(w,h))
            den = float(max(w,h))
            new_cost = w*h + abs(num/den - desired_ratio) * float(1)/desired_ratio * ratio_importance * w * h
            del_cost = new_cost - best_cost
            if del_cost <= 0 or random.random() < math.exp(-float(del_cost) / t):
                exp = new_exp
                if del_cost > 0:
                    uphill += 1
                if new_cost < best_cost:
                    best_w = w
                    best_h = h
                    best_exp = new_exp
                    best_dims = new_dims
                    best_cost = new_cost
            if uphill > N or total_moves > 2*N:
                break
        
        t = r * t

        if (float(reject) / float(total_moves)) > reject_ratio:
            break

        if t < eps:
            break
        
        if (time.time()-time_start)>max_time:
            sys.stderr.write("Max Time Limit Reached\n")
            break

    return (best_exp,(best_w,best_h),best_dims)


def main():
    usage = "usage: %prog [options] filename"
    parser=OptionParser(usage=usage, version="%prog 1.0")

    parser.add_option("-t", "--temp", type="int", dest="t0",
                      help="Initial temperature of the annealing algorithm (override)")
    parser.add_option("-d", "--ratio", type="float", dest="desired_ratio",
                      help="Desired aspect ratio", default=1)
    parser.add_option("-i", "--rimportance", type="float", dest="ratio_importance",
                      help="Ratio importance number (higher=more important)", default=0)
    parser.add_option("-p", "--acceptance", type="int", dest="P",
                      help="Acceptance probability to use for initial temp calculation in %", default=90)
    parser.add_option("-r", "--r", type="float", dest="r",
                      help="r parameter for annealing schedule", default=0.85)
    parser.add_option("-k", "--k", type="int", dest="k",
                      help="moves per temperature", default=5)
    parser.add_option("-e", "--epsilon", type="float", dest="epsilon",
                      help="temperature to stop annealing at", default=1)
    parser.add_option("-x", "--rejectratio", type="float", dest="reject_ratio",
                      help="ratio of rejections causing annealing to terminate", default=0.95)
    parser.add_option("-s", "--stockmeyer", action="store_true", dest="use_stockmeyer",
                      help="use stockmeyer as the cost function", default=False)
    parser.add_option("-m", "--maxtime", dest="max_t",
                      help="maximum time allotted for thermal annealing", default=500)
    parser.add_option("-o", "--iterations", type="int", dest="it",
                      help="number of iterations to anneal", default=1)

    #parse arguments
    (options,args)=parser.parse_args()

    #check for filename being passed
    if len(args) != 1:
        parser.print_help()
        sys.exit(1)

    if options.r>1 or options.r<0:
        sys.exit("R must be between [0,1]")
    
    if options.P>100 or options.P<0:
        sys.exit("P must be between [0,100]")

    if options.k<=0:
        sys.exit("Must have some moves per temperature. K must be >0")

    if options.desired_ratio<0 or options.desired_ratio>1:
        sys.exit("Desired ratio must be between [0,1]")

    
    
    block_dims=[]
    with open(args[0],'r') as f:
        #read expression
        expression=f.readline().replace("\n","").replace("\r","")
        expression_v_h_removed=expression.replace("-V","").replace("-H","")
        num_blocks=max([int(x) for x in expression_v_h_removed.split('-')])+1
        expression=expression.split('-')

        for line in f:
            #read line, replace newlines
            block_dim=line.replace("\n","").replace("\r","")

            #evaluate dimensions as tuple
            block_dim=eval("(" + block_dim + ")")

            #append dimension to list
            block_dims.append(block_dim)

    #make a copy before possible modification of exp (in get initial temp)
    original_exp=list(expression)

    if options.use_stockmeyer:
        global cost_function
        cost_function=stockmeyer
        #generate possible block dims
        new_block_dims=[]
        for block_dim in block_dims:
            block_dim_reversed=list(block_dim)
            block_dim_reversed.reverse()
            block_dim_reversed=tuple(block_dim_reversed)
            if block_dim!=block_dim_reversed:
                new_block_dims.append([block_dim,block_dim_reversed])
            else:
                new_block_dims.append([block_dim])
    else:
        new_block_dims = block_dims

    # if initial temp not specified by the user, get the initial temperature
    if not(options.t0):
        options.t0=get_initial_temperature(expression,new_block_dims, options.P)
        options.t0 = max(170,options.t0)

    ((original_w,original_h),orig_coords)=expression_to_coords(expression,block_dims)
    original_area = original_w * original_h

    t_start = time.time()

    test_exp = expression

    for i in xrange(options.it):
        (new_exp,(best_w,best_h),new_dims) = anneal(test_exp,
                                            options.t0,
                                            options.epsilon,
                                            options.r,
                                            options.k*num_blocks,
                                            options.reject_ratio,
                                            options.desired_ratio,
                                            options.ratio_importance,
                                            new_block_dims,
                                            options.max_t)
        test_exp = new_exp
    
    t_stop = time.time()
    sys.stderr.write("Time: " + str(t_stop-t_start)+"\n")
    best_area = best_w * best_h
    (new_area,new_coords)=expression_to_coords(new_exp,new_dims)
    sys.stderr.write("Original Area: " + str(original_area)+"\n")
    sys.stderr.write("Original Width: " + str(original_w)+"  Original Height: " + str(original_h)+"\n")
    sys.stderr.write("New Area: " + str(best_area) + "\n")
    sys.stderr.write("New Width: " + str(best_w)+"  New Height: " + str(best_h)+"\n")
    print "Original Coordinates:"
    for coord in orig_coords:
        print tuple(coord)
    print "Original Dimensions:"
    for dim in block_dims:
        print dim
    print "New Coordinates:"
    for coord in new_coords:
        print tuple(coord)
    print "New Dimensions:"
    for dim in new_dims:
        print dim
    

if __name__ == '__main__':
    main()
