#!/usr/bin/python2.7

##############################################################################################
# Copyright (c) 2011 Allan Feldman <allan.feldman(__AT__)gatech.edu>                         #
#                                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining                      #
# a copy of this software and associated documentation files (the "Software"),               #
# to deal in the Software without restriction, including without limitation the              #
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell           #
# copies of the Software, and to permit persons to whom the Software is furnished            #
# to do so, subject to the following conditions:                                             #
#                                                                                            #
# The above copyright notice and this permission notice shall be included in all copies      #
# or substantial portions of the Software.                                                   #
#                                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,        #
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   #
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE  #
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       #
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER     #
# DEALINGS IN THE SOFTWARE.                                                                  #
##############################################################################################

# Written By: Allan Feldman

import sys
import time
from coord_gen import expression_to_coords

def stockmeyer(expression,block_dims):
    dim_stack=[]
    possible_stack=[]
    sorted_ind=lambda x: sorted(range(len(x)),key=x.__getitem__)
    for node in expression:
        if node!='V' and node!='H':
            node=int(node)
            node_dim=block_dims[node]
            stack_node_dim = [[(node,dim)] for dim in node_dim]
            #append dimension to dimension stack
            dim_stack.append(stack_node_dim)
            #append possible dims to possible stack
            possible_stack.append(node_dim)
            continue
            
        dim_right=dim_stack.pop()
        dim_left=dim_stack.pop()
        possible_right=possible_stack.pop()
        possible_left=possible_stack.pop()
        if node=='V':
            #sort by x
            sort_key=0
            combiner=lambda left,right: (left[1]==right[1],left[1]>right[1],(left[0]+right[0],max(left[1],right[1])))
        else:
            #sort by y
            sort_key=1
            combiner=lambda top,bottom: (bottom[0]==top[0],top[0]>bottom[0],(max(bottom[0],top[0]),bottom[1]+top[1]))
        
        #pull the dim to sort based on the sort key
        sort_me_left = [dim[sort_key] for dim in possible_left]
        sort_me_right = [dim[sort_key] for dim in possible_right]

        #get indexes to put the correct dimensions in order
        possible_left_ind=sorted_ind(sort_me_left)
        possible_right_ind=sorted_ind(sort_me_right)

        #sort node dim tracking
        dim_left = [dim_left[i] for i in possible_left_ind]
        dim_right = [dim_right[i] for i in possible_right_ind]

        #sort possible dim tracking
        possible_left = [possible_left[i] for i in possible_left_ind]
        possible_right = [possible_right[i] for i in possible_right_ind]

        i=0
        j=0
        new_possibles=[]
        new_dims=[]
        while i<len(possible_left) and j<len(possible_right):
            #combine the possible areas
            (equal,left_gt_right,combined) = combiner(possible_left[i],possible_right[j])

            #new possibility is combined
            new_possibles.append(combined)

            #combined is reached by the dimensions in dim_left[i] and dim_right[j]
            temp_list=[]
            temp_list.extend(dim_left[i])
            temp_list.extend(dim_right[j])
            new_dims.append(temp_list)
            #increment the correct pointer
            if equal:
                i+=1
                j+=1
            elif left_gt_right:
                i+=1
            else:
                j+=1
            
        #push new_possibilities
        possible_stack.append(new_possibles)

        #push new_dims
        dim_stack.append(new_dims)
    
    #the solution should be waiting at the top of the stack :)
    possibles = possible_stack.pop()
    new_dims_list = dim_stack.pop()

    #calculate areas for each dimension
    areas = [dim[0]*dim[1] for dim in possibles]

    #find min area
    min_area = min(areas)

    #min index
    m_index = areas.index(min_area)

    #min dim
    m_dim = possibles[m_index]

    #get block dimensions that result in that area
    new_dims = new_dims_list[m_index]

    #sort new dims by block number
    new_dims.sort()
    
    #delete node number from new_dims
    new_dims = [x[1] for x in new_dims]

    return (m_dim,new_dims)

def main():
    expression=sys.stdin.readline().replace("\n","").replace("\r","")
    expression_v_h_removed=expression.replace("-V","").replace("-H","")
    expression=expression.split('-')
    num_blocks=max([int(x) for x in expression_v_h_removed.split('-')])+1
    block_dims=[]
    for i in xrange(num_blocks):
        block_dim=sys.stdin.readline().replace("\n","").replace("\r","")
        #evaluate dimensions as tuple
        block_dim=eval("(" + block_dim + ")")

        #append dimension to list
        block_dims.append(block_dim)
    
    #generate possible block dims
    new_block_dims=[]
    for block_dim in block_dims:
        block_dim_reversed=list(block_dim)
        block_dim_reversed.reverse()
        block_dim_reversed=tuple(block_dim_reversed)
        if block_dim!=block_dim_reversed:
            new_block_dims.append([block_dim,block_dim_reversed])
        else:
            new_block_dims.append([block_dim])
    
    t_start = time.time()
    ((m_width,m_height),new_dims) = stockmeyer(expression,new_block_dims)
    t_stop = time.time()
    sys.stderr.write("Time: " + str(t_stop-t_start)+"\n")
    min_area = m_width * m_height
    ((original_w,original_h),orig_coords) = expression_to_coords(expression,block_dims)
    original_area = original_w * original_h
    (min_dim_xcheck,new_coords) = expression_to_coords(expression,new_dims)
    min_area_xcheck = min_dim_xcheck[0] * min_dim_xcheck[1]
    if min_area!=min_area_xcheck:
        raise AssertionError
    sys.stderr.write("Original Area: " + str(original_area)+"\n")
    sys.stderr.write("Original Width: " + str(original_w)+"  Original Height: " + str(original_h)+"\n")
    sys.stderr.write("New Area: " + str(min_area) + "\n")
    sys.stderr.write("New Width: " + str(m_width)+"  New Height: " + str(m_height)+"\n")
    print "Original Coordinates:"
    for coord in orig_coords:
        print tuple(coord)
    print "Original Dimensions:"
    for dim in block_dims:
        print dim
    print "New Coordinates:"
    for coord in new_coords:
        print tuple(coord)
    print "New Dimensions:"
    for dim in new_dims:
        print dim

if __name__ == '__main__':
    main()
