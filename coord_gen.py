#!/usr/bin/python2.7

##############################################################################################
# Copyright (c) 2011 Allan Feldman <allan.feldman(__AT__)gatech.edu>                         #
#                                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining                      #
# a copy of this software and associated documentation files (the "Software"),               #
# to deal in the Software without restriction, including without limitation the              #
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell           #
# copies of the Software, and to permit persons to whom the Software is furnished            #
# to do so, subject to the following conditions:                                             #
#                                                                                            #
# The above copyright notice and this permission notice shall be included in all copies      #
# or substantial portions of the Software.                                                   #
#                                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,        #
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   #
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE  #
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       #
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER     #
# DEALINGS IN THE SOFTWARE.                                                                  #
##############################################################################################

# Written By: Allan Feldman

def expression_to_coords(expression,block_dims):
    stack=[]
    for node in expression:
        if node!='V' and node!='H':
            node=int(node)
            #track the block dimensions as they come up the stack
            #also track (node#,coord)
            stack_item=[block_dims[node],[(node,[0,0])]]
            stack.append(stack_item)
            continue
        
        #pop 2
        (right_dim,right_coord)=stack.pop()
        (left_dim,left_coord)=stack.pop()

        #calculate the new dimension of this node when combined
        if node=='V':
            this_node_dim=(left_dim[0]+right_dim[0],max(left_dim[1],right_dim[1]))
        else:
            this_node_dim=(max(left_dim[0],right_dim[0]),left_dim[1]+right_dim[1])

        #modify the right side to be to the right of the left side
        #if node is H, add left height to right y coord
        #if node is V, add left width to right x coord
        if node=='H':
            for coord in left_coord:
                coord[1][1]+=right_dim[1]
        else:
            for coord in right_coord:
                coord[1][0]+=left_dim[0]
        
        #add all coordinates from left and right to 1 list
        left_coord.extend(right_coord)

        #push updated block_dim and coord list to stack
        stack.append([this_node_dim,left_coord])

    #result is on top of the stack
    (final_dim,coords)=stack.pop()

    #order by node number
    coords.sort()
    coords = [x[1] for x in coords]

    return (final_dim, coords)
