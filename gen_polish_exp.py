#!/usr/bin/python2.7

##############################################################################################
# Copyright (c) 2011 Allan Feldman <allan.feldman(__AT__)gatech.edu>                         #
#                                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining                      #
# a copy of this software and associated documentation files (the "Software"),               #
# to deal in the Software without restriction, including without limitation the              #
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell           #
# copies of the Software, and to permit persons to whom the Software is furnished            #
# to do so, subject to the following conditions:                                             #
#                                                                                            #
# The above copyright notice and this permission notice shall be included in all copies      #
# or substantial portions of the Software.                                                   #
#                                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,        #
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   #
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE  #
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       #
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER     #
# DEALINGS IN THE SOFTWARE.                                                                  #
##############################################################################################

# Written By: Allan Feldman

import random,sys
def good_expression(exp):
    num_operand=0
    num_operator=0
    for e in exp:
        if e=='*':
            num_operator+=1
        else:
            num_operand+=1
        if num_operator>=num_operand:
            return False
    return True

def gen_polish_expression(num_nodes):
    exp=list('*'*(num_nodes-1))
    exp.extend([str(x) for x in xrange(num_nodes)])
    while not(good_expression(exp)):
        random.shuffle(exp)

    chain=False
    co = 0
    ops = ['H','V']
    for i in xrange(len(exp)):
        node = exp[i]
        if node=='*':
            if chain:
                exp[i]=ops[co]
                
            else:
                chain=True
                co = random.randint(0,1)
                exp[i]=ops[co]
            co^=1
        else:
            chain=False
    return '-'.join(exp)

if __name__ == '__main__':
    exp=gen_polish_expression(int(sys.argv[1]))
    print exp
